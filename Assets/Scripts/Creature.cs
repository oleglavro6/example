using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : MonoBehaviour
{
    private pointTree pointTree;
    [SerializeField] private float _step;
    private float _progress;
    private Vector2 startPoint;
    private Vector2 endPoint;

    [SerializeField] int _health;
    [SerializeField] int _damage;
    

    
    void Start()
    {
        pointTree = pointTree.instance;

        startPoint = transform.position;
        endPoint = pointTree.transform.position;
    }


    void Update()
    {

         transform.position = Vector2.MoveTowards(startPoint, endPoint, _progress);
        _progress += _step;
    }
    void OnMouseDown()
    {
        GetDamage();
        if (_health <= 0) Die();
    }

    void GetDamage()
    {
        _health--;
    }
    void TakeDamage()
    {

    }
    

    public void Die()
    {
        Destroy(this.gameObject);
    }
}
