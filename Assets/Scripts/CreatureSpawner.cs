using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureSpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> _creature;
    [SerializeField] private List<Transform> _spawnPoints;
    [SerializeField] private float _spawnTime=4f;
    private float _timer;


    void Start()
    {
        _timer = _spawnTime;
    }

    
    void Update()
    {
        if (_timer <= 0)
        {
            GameObject gm = Instantiate(_creature[Random.Range(0, _creature.Count)], _spawnPoints[Random.Range(0, _spawnPoints.Count)].position, Quaternion.identity);
            _timer = _spawnTime;
        }
        else
        {
            _timer -= Time.deltaTime;
        }
    }
}
