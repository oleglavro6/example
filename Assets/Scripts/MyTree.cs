using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTree : MonoBehaviour
{
    public List<Sprite> Sprites;
    public SpriteRenderer sp;
    private float _timer;
    private int _index;
    private int _delay;


    void Start()
    {
        sp = GetComponent<SpriteRenderer>();
    }


    void Update()
    {
        _timer = Time.time;
        //Debug.Log(_timer);
        if (_index == 0)
        {
            _delay = 10;
        }
        else
        {
            _delay = 20;
        }
        if (_timer > _delay)
        {
            sp.sprite = Sprites[_index];
            if (_index < 1) _index++;
        }
    }   
}
